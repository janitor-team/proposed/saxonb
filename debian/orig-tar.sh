#!/bin/sh -e

VERSION=$2
TAR=../saxonb_$VERSION+dfsg.orig.tar.gz
DIR=saxonb-$VERSION
TAG="$VERSION"

svn export https://saxon.svn.sourceforge.net/svnroot/saxon/tags/${TAG}/ $DIR
tar -c -z -f $TAR --exclude '*.class' --exclude 'build' --exclude 'bn' --exclude 'bj/net/sf/saxon/dotnet' --exclude 'bj/javax' --exclude 'bj/net/sf/saxon/xqj' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
